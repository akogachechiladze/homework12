package com.example.usaid121021jsonparcinghomework12

import com.example.json.*
import com.google.gson.annotations.SerializedName

data class Requests(
    val id:String?,
    val projectId:String?,
    val equipmentId:String?,
    val status:String?,
    val requestedBy:String?,
    val acceptedBy:String?,
    val author:String?,
    val category:String?,
    val locations: Locations?,
    val filters: List<Filters>?,
    val type: String?,
    val organization: String?,
    val address: String?,
    val startDate: String?,
    val endDate:String?,
    val description: String?,
    val prolongDates: List<Any>?,
    val releaseDates: List<Any>?,
    val isDummy: Boolean?,
    val hasDriver:Boolean?,
    val overwriteDate:String?,
    val metaInfo:String?,
    val warehouseId:String?,
    val rentalDescription:String?,
    val startDateMilliseconds:Int?,
    val endDateMilliseconds:Int?,
    val equipment: List<Equipment>








){
    data class Locations(
        val type:String?,
        val coordinates: List<Double>?
    )




    data class Filters(
        val name: String?,
        val value: Value?
        )


    data class Value(
        val max:Int?,
        val min:Int?
    )

    data class InternalTransportations(
        val id:String?,
        val projectRequestId:String?,
        val pickUpDate:String?,
        val deliveryDate:String?,
        val description:String?,
        val status:String?,
        val startDateOption:String?,
        val endDateOption:String?,
        val pickUpLocation: Requests.pickUpLocation?,
        val deliveryLocation: Requests.DeliveryLocation,
        val provider:String?,
        val pickUpLocationAddress:String?,
        val deliveryLocationAddress:String?,
        val pGroup:String?,
        val isOrganizedWithoutSam:String?,
        val templatePGroup:String?,
        val pickUpDateMilliseconds:Int?,
        val deliveryDateMilliseconds:Int?,
        val startDateOptionMilliseconds:String?,
        val endDateOptionMilliseconds:String?,


    )

    data class pickUpLocation(
        val type:String?,
        val coordinates:List<Double>
    )

    data class DeliveryLocation(
        val type:String?,
        val coordinates:List<Double>
    )

    data class Equipment(
        val id:String?,
        val title:String?,
        val invNumber:String?,
        val categoryId: String?,
        val modelId:String?,
        val brandId:String?,
        val year:Int?,
        val specifications: Requests.Specifications?,
        val weight: Int?,
        @SerializedName("additional_specification")
        val additionalSpecifications:String?,
        val structureId: String?,
        val organizationId:String?,
        val beaconType:String?,

        val beaconId:String?,
        val beaconVendor:String?,
        val RFID:String?,
        val dailyPrice:String?,
        val inactive:String?,
        val tag:Requests.Tag?,
        val telematicBox:String?,
        val createdAt:String?,
        val special_number:String?,
        @SerializedName("last_check")
        val lastCheck: String?,
        val nextCheck: String?,
        @SerializedName("responsible_person")
        val responsiblePerson: Any?,
        @SerializedName("test_type")
        val testType: Any?,
        @SerializedName("unique_equipment_id")
        val uniqueEquipmentId: String?,
        @SerializedName("bgl_number")
        val bglNumber: String?,
        @SerializedName("serial_number")
        val inventory:String?,
        val warehouseId:String?,
        val trackingTag:String?,
        val workingHours:String?,
        @SerializedName("navaris_criteria")
        val navarisCriteria:String?,
        @SerializedName("dont_send_to_as400")
        val dontSendToAs400:Boolean?,
        val model:Requests.Model?,

        val brand: com.example.json.Brand?,
        val category: Category?,
        val structure: Structure?,
        val wareHouse: Any?,
        val equipmentMedia: EquipmentMedia?,
        val telematics: Telematics?,
        val isMoving: Boolean?


    )

    data class Specifications(
        val key:String?,

        val value:String?
    )



    data class Tag(
        val date: String?,
        val authorName: String?,
        val media: List<Any>?
    )

    data class Model(
        val id: String?,
        val name: String?,
        val createdAt: String?,
        val brand: Requests.Brand?,
    )




    data class Brand(
        val id: String?,
        val name: String?,
        val createdAt: String?
    )

    data class Category(
        val id: String?,
        val name: String?,
        @SerializedName("name_de")
        val nameDe: String?,
        val createdAt: String?,
        val media: List<Any?>
    )

    data class Structure(
        val id: String?,
        val name: String?,
        val type: String?,
        val color: String?
    )



    data class EquipmentMedia(
        val id: String?,
        val name: String?,
        val files: Files?,
        val type: String?,
        val modelId: String?,
        val main: Boolean?,
        val modelType: String?,
        val createdAt: String?
    )

    data class Files(
        val size: String?,
        val path: String?
    )



    data class Telematics(
        val timestamp: Int?,
        val eventType: String?,
        val projectId: String?,
        val equipmentId: String?,
        val locationName: String?,
        val location: Location?,
        val costCenter: String?,
        val lastLatitude: String?,
        val lastLongitude: String?,
        val lastLatLonPrecise: String?,
        val lastAddressByLatLon: String?,
    )
    data class Location(
        val type: String?,
        val coordinates: List<Double?>
    )






}

